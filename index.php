<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/index.css">
    <title>Index</title>
</head>

<body>
    <header>
        <label for="check">
            <i class="fas fa-bars" id="sidebar_btn"></i>
        </label>
        <div class="left-area">
            <h3>EM <span>Tools</span> </h3>
        </div>
        <div class="right-area">
            <a href="admin-connexion.php" class="logout-btn"><i class="fas fa-user"></i>LogIn Admin</a>
            <a href="user-connexion.php" class="logout-btn"><i class="fas fa-user"></i>LogIn User</a>
        </div>
    </header>
    <section>
        <h1>
            <span class="sp1">W</span>
            <span class="sp2">E</span>
            <span class="sp3">L</span>
            <span class="sp4">C</span>
            <span class="sp5">O</span>
            <span class="sp6">M</span>
            <span class="sp7">E</span>
        </h1>
    </section>
    <footer>
        <div class="footer-head">
            <span><i class="fas fa-facebook facebook"></i></span>
            <span><i class="fas fa-twitter twitter"></i></span>
            <span><i class="fas fa-instagram instagram"></i></span>
            <span><i class="fas fa-telegram telegram"></i></span>
            <span><i class="fas fa-google-plus gp"></i></span>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0">
                                    Copyright &copy;<script>
                                        document.write(new Date().getFullYear());
                                    </script> All rights reserved | This website is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="#">EMTools</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
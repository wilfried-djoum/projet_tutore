<?php 
    session_start();
    
    $bdd = new PDO('mysql:host=localhost;dbname=testeur', 'root', '');

    if(isset($_POST['formconnexion']))
    {
        $nameconnect = $_POST['nameconnect'];
        $mailconnect = $_POST['mailconnect'];
        $mdpconnect = $_POST['mdpconnect'];
        if(!empty($nameconnect) AND !empty($mailconnect) AND !empty($mdpconnect))
        {
            $requser = $bdd->prepare("SELECT * FROM etudiant WHERE nom-etudiant = ?
            AND email-etudiant = ?
            AND mot-de-passe = ?");
            $requser->execute(array($nameconnect, $mailconnect, $mdpconnect));
            $userexist = $requser->rowCount();

            if($userexist == 1)
            {
                $userinfo = $requser->fetch();
                $_SESSION['id'] = $userinfo['id-etudiant'];
                $_SESSION['nom'] = $userinfo['nom-etudiant'];
                $_SESSION['email'] = $userinfo['email-etudiant'];
                header("Location: dashboard-user.php?id=" .$_SESSION['id']);

            }
            else{
                $erreur = "Mauvais Identifiants !";
            }
        }
        else{
            $erreur = 'Tous les champs doivent etre complétés !';
        }
        // if($nameconnect != $_SESSION['nom-etudiant'] OR $mailconnect != $_SESSION['email-etudiant'] OR $mdpconnect != $_SESSION['mot-de-passe'])
        // {
        //     $erreur = "Veuillez entrer les bons Identifiants !";
        // }
    }
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/user-connexion.css">
    <title>User-Connexion</title>
</head>

<body>
    <div class="container">
        <div class="mycard">
            <div class="row">
                <div class="col-md-6">
                    <div class="myLeftCtn">
                        <form class="myForm text-center" method="POST" action="">
                            <header>Login User</header>
                            <div class="form-group">
                                <i class="fas fa-user"></i>
                                <input class="myInput" type="text" placeholder="Name" name="nameconnect" id="username">
                                <div class="invalid-feedback">Please fill out this field.
                                </div>
                            </div>

                            <div class="form-group">
                                <i class="fas fa-envelope"></i>
                                <input class="myInput" placeholder="Email" type="email" name="mailconnect" id="email">
                            </div>

                            <div class="form-group">
                                <i class="fas fa-lock"></i>
                                <input class="myInput" placeholder="Password" type="password" name="mdpconnect" id="password">
                            </div>

                            <div class="form-group">
                                <label for=""><input id="check_1" type="checkbox" name="check_1" id="" required><small>I read and agree to Terms & Conditions</small></input>
                                    <div class="invalid-feedback">You must check the box.</div>
                                </label>
                            </div>

                            <input type="submit" name="formconnexion" class="butt" value="LogIn !">

                        </form>
                        <?php
                            if(isset($erreur))
                            {
                                echo '<center><font family="segoe"><font size ="4px"><font color="red">'.$erreur .'</font></font></font></center>';
                            }
                        ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="myRightCtn">
                        <div class="box">
                            <header>Hello World !</header>

                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corporis harum quasi ab, facere provident dolore facilis praesentium et totam! Accusamus ratione aliquid ex quas obcaecati aut beatae repudiandae quis error.
                                Optio, eos? Nesciunt molestias accusantium iure, laboriosam expedita unde quis perspiciatis molestiae quidem officiis, mollitia ipsam quibusdam veniam! Eos cum explicabo omnis ratione. Eius iure est maxime iusto labore in.</p>
                            
                            <a href="admin-connexion.php" class="butt_out">Admin Connexion</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
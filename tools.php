<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
    integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
    integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
    crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/tools.css">
    <title>Tools</title>
</head>
<body>
    <input type="checkbox" id="check">

    <header>
        <label for="check">
            <i class="fas fa-bars" id="sidebar_btn"></i>
        </label>
        <div class="left-area">
            <h3>EM <span>Tools</span> </h3>
        </div>
        <div class="right-area">
            <a href="#" class="logout-btn">Logout</a>
        </div>
    </header>

    <div class="sidebar">
        <center>
            <img src="photos/images.jpg" class="profile_image" alt="">
            <h4>NoName</h4>
        </center>
        <a href="dashboard-user.html" class="dash"> <span>Dashboard</span><i class="fas fa-desktop"></i> </a>
        <a href="" class="tools"> <span>Tools</span><i class="fas fa-cogs"></i> </a>
        <a href="list.html"><span>My List</span> <i class="fas fa-shopping-cart"><span class="number">0</span></i></a>
        <a href="contact.html"><span>Contact</span><i class="fas fa-phone"></i></a>
        <a href="about.html"><span>About</span><i class="fas fa-info-circle"></i></a>
        <a href="setting.html"><span>Settings</span><i class="fas fa-desktop"></i></a>
    </div>
</body>
</html>
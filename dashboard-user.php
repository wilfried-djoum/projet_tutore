<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="css/dashboard-user.css">
  <title>Dashboard-user</title>
</head>

<body class="body">

  <input type="checkbox" id="check">

  <header>
    <label for="check">
      <i class="fas fa-bars" id="sidebar_btn"></i>
    </label>
    <div class="left-area">
      <h3>EM <span>Tools</span> </h3>
    </div>
    <div class="right-area">
      <a href="#" class="logout-btn">Logout</a>
    </div>
  </header>

  <div class="sidebar">
    <center>
      <img src="photos/images.jpg" class="profile_image" alt="">
      <h4>MyName</h4>
    </center>
    <a href="dashboard.php" class="dash"> <span>Dashboard</span> <i class="fas fa-desktop"></i></a>
    <a href="tools.php"> <span>Tools</span> <i class="fas fa-cogs"></i></a>
    <a href="list.php"> <span>My List</span> <i class="fas fa-shopping-cart"><span class="number">0</span></i></a>
    <a href="contact.php"><span>Contact</span> <i class="fas fa-phone"></i></a>
    <a href="about.php"><span>About</span> <i class="fas fa-info-circle"></i></a>
    <a href="" class="modify"><span>Modify</span> <i class="fas fa-desktop"></i></a>
  </div>
  <div class="container contenu">
    <div class="row row-cols-1 row-cols-md-3 g-4">
      <div class="col">
        <div class="card h-100">
          <img src="photos/cable.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Cables</h5>
            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-100">
          <img src="photos/arduino.jpg" class="card-img-top" height="61%" object-fit="content" alt="...">
          <div class="card-body">
            <h5 class="card-title">Arduino Kits</h5>
            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-100">
          <img src="photos/modem.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Connexion Kits</h5>
            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-100">
          <img src="photos/balazs-ketyi-6ba_vdgx_go-unsplash.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-100">
          <img src="photos/balazs-ketyi-6ba_vdgx_go-unsplash.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-100">
          <img src="photos/balazs-ketyi-6ba_vdgx_go-unsplash.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="filigrammenoir">
  <div class="container resetForm">
  <i class="fas fa-times lacroix"></i>
    <div class="mycard">
      <div class="row">
        <div class="col-md-6">
          <div class="myLeftCtn">
            <form class="myForm text-center" method="POST" action="">
              <div class="header">Form Update</div>
              <div class="form-group">
                <input class="myInput" type="text" placeholder="Surname" name="namemodified" id="nom">
                <div class="invalid-feedback">Please fill out this field.
                </div>
              </div>
              <div class="form-group">
                <input class="myInput" type="text" placeholder="Firstname" name="prenommodified" id="prenom">
                <div class="invalid-feedback">Please fill out this field.
                </div>
              </div>

              <div class="form-group">
                <input class="myInput" placeholder="NewPassword" type="password" name="passwordmodified" id="password">
              </div>

              <div class="form-group">
                <input class="myInput" placeholder="NewEmail" type="email" name="mailmodified" id="email">
              </div>

              <div class="form-group">
                <input class="myInput" type="file" placeholder="Firstname" name="avatar" id="avatar">
              </div>
          </div>
          <input type="submit" name="formmodify" class="butt" value="Modify !">
          <input type="submit" name="formcancel" class="cancel" value="Cancel ! ">
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="js/user-dashboard.js"></script>
</body>

</html>